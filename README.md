## This module provides dynamic layout

1. Enable module.
2. Add section to manager layout
3. Add region and classes.

![Example](https://i.imgur.com/PGD9kgH.png)
